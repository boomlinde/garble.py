#!/usr/bin/env python

import re
import random
import sys

def reindent(match):
    return ''.join([random.choice(['\t', ' ' * 8]) for c in match.group(1)])

for line in sys.stdin:
    sys.stdout.write(re.sub(r'^([\t ]*)', reindent, line))

